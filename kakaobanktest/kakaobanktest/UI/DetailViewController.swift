//
//  DetailViewController.swift
//  kakaobanktest
//
//

import AVKit
import UIKit

class DetailViewController: UIViewController {
    @IBOutlet var artWorkImage: UIImageView!
    @IBOutlet var detailLabel: VerticalAlignLabel! // 수직 정렬이 가능한 Custom UILabel 사용
    @IBOutlet var previewButton: UIButton!
    @IBOutlet var artistLabel: UILabel!
    @IBOutlet var artistButton: UIButton!
    @IBOutlet var collectionLabel: UILabel!
    @IBOutlet var collectionButton: UIButton!
    @IBOutlet var trackLabel: UILabel!
    @IBOutlet var trackButton: UIButton!

    var data: ITunesContentsData!

    override func viewDidLoad() {
        super.viewDidLoad()

        if data.previewUrl == nil { previewButton.isHidden = true }
        if data.artistViewUrl == nil { artistButton.isHidden = true }
        if data.collectionViewUrl == nil { collectionButton.isHidden = true }
        if data.trackViewUrl == nil { trackButton.isHidden = true }

        if let artWorkUrl = data.artworkUrl100 {
            artWorkImage.image = AppStoreData.loadImage(url: artWorkUrl)
        }

        if let track = data.trackName { trackLabel.text = "\(track)" }
        if let artist = data.artistName { artistLabel.text = "Artist : \(artist)" }
        if let collection = data.collectionName { collectionLabel.text = "Collection : \(collection)" }

        var detailText = ""
        if let kind = data.kind { detailText += "Kind : \(kind)\n" }
        if let genre = data.primaryGenreName { detailText += "Genre : \(genre)\n" }
        if let releaseDate = data.releaseDate { // http://monibu1548.github.io/2018/05/13/string-date-convert/
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            detailText += "ReleaseDate : \(dateFormatter.string(from: releaseDate))\n"
        }
        if let country = data.country { detailText += "Country : \(country)\n" }
        if let currency = data.country { detailText += "Currency : \(currency)\n" }
        if let trackPrice = data.trackPrice { detailText += "Track Price : \(trackPrice)\n" }
        if let collectionPrice = data.collectionPrice { detailText += "Collection Price : \(collectionPrice)\n" }

        detailLabel.text = detailText
    }

    @IBAction func backButtonTouchUp(_: UIButton) {
        dismiss(animated: false, completion: nil)
    }

    @IBAction func previewButtonTouchUp(_: UIButton) {
        guard let previewUrl = data.previewUrl else { return }

        // https://stackoverflow.com/questions/25932570/how-to-play-video-with-avplayerviewcontroller-avkit-in-swift
        // go to preview video play using AVPlayerViewController
        let player = AVPlayer(url: previewUrl)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        present(playerViewController, animated: true) {
            playerViewController.player?.play()
        }
    }

    @IBAction func openButtonTouchUp(_: UIButton) {
        guard let trackUrl = data.trackViewUrl else { return }
        UIApplication.shared.open(trackUrl, options: [:], completionHandler: nil)
    }

    @IBAction func artistButtonTouchUp(_: UIButton) {
        guard let artistUrl = data.artistViewUrl else { return }
        UIApplication.shared.open(artistUrl, options: [:], completionHandler: nil)
    }

    @IBAction func collectionButtonTouchUp(_: UIButton) {
        guard let collectionUrl = data.collectionViewUrl else { return }
        UIApplication.shared.open(collectionUrl, options: [:], completionHandler: nil)
    }
}
