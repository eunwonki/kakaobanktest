//
//  ViewController.swift
//  kakaobanktest
//
//

import UIKit

enum SearchState {
    case before
    case searching
    case searched
}

class ViewController: UIViewController {
    @IBOutlet var searchLabel: UILabel!
    @IBOutlet var searchBar: UISearchBar!

    @IBOutlet var historyView: UIView!

    @IBOutlet var historyTableView: UITableView!
    @IBOutlet var historySelectTableView: UITableView!
    @IBOutlet var dataPreviewTableView: UITableView!

    let detailViewControllerIdentifier = "DetailViewController"
    let dataPreviewTableViewCellIdenrifier = "DataPreviewTableViewCell" // 검색 결과 itunes 검색 결과의 간략한 정보 제공 tableviewcell

    var data: [ITunesContentsData] = [] // 검색 이후 itunes contents 결과를 저장.
    var histories: [History] = [] // 검색시 실시간 로컬에 최근 기록으로 반영.
    var selected: [History] = [] // 검색 결과와 키워드가 겹치는 목록 추출. 검색 중일때 키워드 검출용.

    override func viewDidLoad() {
        super.viewDidLoad()

        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).title = "취소" // for custom cancel button text in searchbar

        histories = AppStoreData.getLocalHistory()

        historyTableView.delegate = self
        historyTableView.dataSource = self

        historySelectTableView.delegate = self
        historySelectTableView.dataSource = self

        dataPreviewTableView.delegate = self
        dataPreviewTableView.dataSource = self
    }

    func loadItunesDataAsync(_ text: String, _ completion: @escaping () -> Void) {
        AppStoreData.loadData(keyword: text) { [weak self] data in
            self?.data = data
            completion()
        }
    }

    func getSelectHistory(_ keyword: String?) {
        guard let keyword = keyword else { return }

        selected = []

        for h in histories {
            if h.text.lowercased().contains(keyword.lowercased()) { // case-insensitive
                selected.append(h)
            }
        }
    }

    func setSearchState(newState: SearchState) {
        switch newState {
        case .before:
            historyTableView.reloadData()

            searchBar.showsCancelButton = false
            searchBar.text = ""
            searchBar.resignFirstResponder()

            searchLabel.isHidden = false
            historyView.isHidden = false
            historySelectTableView.isHidden = true
            dataPreviewTableView.isHidden = true
        case .searching:
            historySelectTableView.reloadData()

            searchBar.showsCancelButton = true

            searchLabel.isHidden = true
            historyView.isHidden = true
            historySelectTableView.isHidden = false
            dataPreviewTableView.isHidden = false
        case .searched:
            dataPreviewTableView.reloadData()

            searchBar.resignFirstResponder()
            searchBar.showsCancelButton = true

            searchLabel.isHidden = true
            historyView.isHidden = true
            historySelectTableView.isHidden = true
            dataPreviewTableView.isHidden = false
        }
    }

    func notifyMessage(title: String, message: String = "") {
        // alertcontroller를 사용하면 app이 freeze 된다는 단점이 있지만 눈에 확 띄고 사용이 간편하여 사용하였습니다.
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(alertController, animated: false)
    }

    /// 검색시에 키워드 일치를 적용.  일치하는 부분의 글자색을 변경.
    public static func keywordMatchColoring(keyword: String?, label: UILabel?) {
        // System 색상이나 label 색상을 사용해야 ios13부터 적용된 다크모드 라이트모드에 따라 자동으로 색상이 변경됨.
        label?.textColor = UIColor.systemGray // sourceColor를 잃지 않도록 Default Color를 항상 명시.
        guard let text = label?.text, let keyword = keyword else { return }

        let attributedStr = NSMutableAttributedString(string: text)
        attributedStr.addAttribute(.foregroundColor, value: UIColor.label, range: (text.lowercased() as NSString).range(of: keyword.lowercased()))

        label?.attributedText = attributedStr
    }
}

extension ViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text, text != "" else { return }

        // save history

        histories.insert(History(Date: Date(), text: text), at: 0)
        AppStoreData.saveLocalHistory(histories)

        // load contents data

        loadItunesDataAsync(text) {
            DispatchQueue.main.async { [weak self] in // UI process work correctly only in main thread
                guard let self = self else { return }
                if self.data.isEmpty {
                    self.notifyMessage(title: "\(text)의 검색 결과를 얻을 수 없습니다.")
                }
                self.setSearchState(newState: .searched)
            }
        }
    }

    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        getSelectHistory(searchBar.text)
        setSearchState(newState: .searching)
    }

    func searchBar(_: UISearchBar, textDidChange searchText: String) {
        getSelectHistory(searchText)
        setSearchState(newState: .searching)
    }

    func searchBarCancelButtonClicked(_: UISearchBar) {
        setSearchState(newState: .before)
    }
}
