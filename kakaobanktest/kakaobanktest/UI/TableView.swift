//
//  HistoryTableView.swift
//  kakaobanktest
//
//

import UIKit

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    // https://stackoverflow.com/questions/16195660/multiple-uitableview-in-single-viewcontroller
    // multiple table view in one viewcontroller
    // 하나의 tableview에 구현하고 싶었지만 독립적인 부분이 많아 각각의 tableview로 구현.

    func tableView(_ tableView: UITableView, numberOfRowsInSection _: Int) -> Int {
        switch tableView {
        case historyTableView:
            return histories.count
        case historySelectTableView:
            return selected.count
        case dataPreviewTableView:
            return data.count
        default: return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView {
        case historyTableView:
            let cell = UITableViewCell()

            cell.textLabel?.text = histories[indexPath.row].text
            cell.textLabel?.textColor = UIColor.systemBlue

            return cell

        case historySelectTableView:
            let cell = UITableViewCell()

            cell.textLabel?.text = selected[indexPath.row].text
            ViewController.keywordMatchColoring(keyword: searchBar.text, label: cell.textLabel)

            return cell

        case dataPreviewTableView:
            guard let cell = dataPreviewTableView.dequeueReusableCell(withIdentifier: dataPreviewTableViewCellIdenrifier, for: indexPath) as? DataPreviewTableViewCell else {
                return UITableViewCell()
            }

            cell.setData(data[indexPath.row])

            return cell

        default: return UITableViewCell()
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch tableView {
        case historyTableView, historySelectTableView:
            searchBar.text = tableView.cellForRow(at: indexPath)?.textLabel?.text
            searchBarSearchButtonClicked(searchBar)

        case dataPreviewTableView: // go to detail view
            guard let vc = self.storyboard?.instantiateViewController(withIdentifier: detailViewControllerIdentifier) as? DetailViewController
            else {
                fatalError("\(#function) : DetailViewController Load Fail")
            }

            vc.data = data[indexPath.row]
            vc.modalPresentationStyle = .fullScreen
            present(vc, animated: false, completion: nil)

        default:
            return
        }
    }
}

class DataPreviewTableViewCell: UITableViewCell {
    @IBOutlet var artworkImageView: UIImageView!
    @IBOutlet var kindLabel: UILabel!
    @IBOutlet var artistLabel: UILabel!
    @IBOutlet var collectionLabel: UILabel!
    @IBOutlet var trackLabel: UILabel!

    var trackViewUrl: URL?

    public func setData(_ data: ITunesContentsData) {
        if let artWorkUrl = data.artworkUrl60 {
            artworkImageView.image = AppStoreData.loadImage(url: artWorkUrl)
        }

        if let kind = data.kind {
            kindLabel.text = "kind : \(kind)"
        }

        if let artist = data.artistName {
            artistLabel.text = "Artist : \(artist)"
        }

        if let collection = data.collectionName {
            collectionLabel.text = "Collection : \(collection)"
        }

        if let track = data.trackName {
            trackLabel.text = "Track : \(track)"
        }

        if let trackUrl = data.trackViewUrl {
            trackViewUrl = trackUrl
        }
    }

    @IBAction func openButtonTouchUp(_: UIButton) {
        guard let trackUrl = trackViewUrl else { return }
        // https://stackoverflow.com/questions/8755843/how-to-open-iphones-default-browser
        // go to webbrowser
        UIApplication.shared.open(trackUrl, options: [:], completionHandler: nil)
    }
}
