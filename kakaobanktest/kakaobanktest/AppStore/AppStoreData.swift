//
//  AppStoreData.swift
//  kakaobanktest
//
//

import Foundation
import UIKit

public struct Result: Codable {
    public var resultCount: Int
    public var results: [ITunesContentsData]
}

/// itunes 검색 결과 자세한 데이터 spec은 아래 링크 참조
/// https://affiliate.itunes.apple.com/resources/documentation/itunes-store-web-service-search-api/
public struct ITunesContentsData: Codable {
    public var wrapperType: String
    public var collectionExplicitness: String?
    public var trackExplicitness: String?
    public var kind: String?
    public var trackName: String?
    public var artistName: String?
    public var collectionName: String?

    public var artistViewUrl: URL?
    public var collectionViewUrl: URL?
    public var trackViewUrl: URL?
    public var previewUrl: URL?

    public var collectionCensoredName: String?
    public var trackCensoredName: String?
    public var artworkUrl30: URL?
    public var artworkUrl60: URL?
    public var artworkUrl100: URL?
    public var releaseDate: Date?
    public var country: String?
    public var currency: String?
    public var primaryGenreName: String?
    public var collectionPrice: Float?
    public var trackPrice: Float?
    public var contentAdvisoryRating: String?
    public var isStreamable: Bool?
}

/// 검색 기록
public struct History: Codable {
    public var Date: Date
    public var text: String
}

public class AppStoreData {
    private static let historyFileName = "history.json"
    private static let itunesSearchApiUrl = "https://itunes.apple.com/search" // itunes api endpoint

    /// load image from url
    public static func loadImage(url: URL) -> UIImage? {
        do {
            let data = try Data(contentsOf: url)
            return UIImage(data: data)
        } catch {
            NSLog("\(#function) : image load failed(\(url)")
            return nil
        }
    }

    /// load itunes contents data from api
    public static func loadData(keyword: String, completion: @escaping ([ITunesContentsData]) -> Void) {
        guard let url = URL(string: urlAddingTerm(keyword)) else { return }

        download(url) { data, error in
            guard let data = data else {
                NSLog("\(#function) : data download fail(\(url) : \(error.debugDescription)")
                return
            }

            guard let result = deserializeResult(data)?.results else {
                NSLog("\(#function) : Itunes data json parsing fail")
                return
            }

            completion(result)
        }
    }

    public static func getLocalHistory() -> [History] {
        let url = getJsonFilePath(fileName: historyFileName)
        do {
            let data = try Data(contentsOf: url)
            guard let histories = deserializeHsitory(data) else {
                NSLog("\(#function) : History data json parsing fail")
                return []
            }
            return histories
        } catch {
            NSLog("\(#function) : History data load fail")
            return []
        }
    }

    public static func saveLocalHistory(_ histories: [History]) {
        do {
            guard let data = serialize(histories) else {
                NSLog("\(#function) : data serialize fail")
                return
            }
            let url = getJsonFilePath(fileName: historyFileName)

            try data.write(to: url)
        } catch {
            NSLog("History data save failed")
        }
    }

    /// 입력받은 keyword를 itunes data api의 term 에 맞게 변환,
    private static func urlAddingTerm(_ keyword: String) -> String {
        // https://affiliate.itunes.apple.com/resources/documentation/itunes-store-web-service-search-api/
        // term 규칙 참고.
        var term = keyword.replacingOccurrences(of: " ", with: "+") // space to `+`

        term = term.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        return AppStoreData.itunesSearchApiUrl + "?term=" + term
    }

    private static func deserializeHsitory(_ from: Data) -> [History]? {
        return deserialize(from)
    }

    private static func deserializeResult(_ from: Data) -> Result? {
        return deserialize(from)
    }

    /// Json Deserialize
    private static func deserialize<T: Codable>(_ from: Data) -> T? {
        do {
            let decoder = JSONDecoder()
            // https://stackoverflow.com/questions/28748162/how-do-i-format-json-date-string-with-swift
            // iso 8601 date format example : 2015-02-27T11:24:00Z
            decoder.dateDecodingStrategy = .iso8601
            return try decoder.decode(T.self, from: from)
        } catch {
            return nil
        }
    }

    /// Json Serialize
    private static func serialize<T: Codable>(_ from: T) -> Data? {
        do {
            let encoder = JSONEncoder()
            encoder.dateEncodingStrategy = .iso8601
            return try encoder.encode(from)
        } catch {
            return nil
        }
    }

    /// 웹에서 데이터 다운로드. 비동기로 실행.
    private static func download(_ url: URL, _ completion: @escaping (Data?, Error?) -> Void) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let statusCode = (response as? HTTPURLResponse)?.statusCode else {
                NSLog("\(#function)(\(url)) : Response is nil")
                completion(nil, NSError())
                return
            }
            guard statusCode == 200 else {
                NSLog("\(#function)(\(url)) : HTTP Response Error Status Code(\(statusCode))")
                completion(nil, error)
                return
            }
            guard error == nil else {
                NSLog("\(#function)(\(url)) : Communication Error(\(error.debugDescription)")
                completion(nil, error)
                return
            }
            completion(data, error)
        }.resume()
    }

    /// Json 파일의 전체 경로 리턴.
    private static func getJsonFilePath(fileName: String) -> URL { // Rule : 로컬 Json 파일을 Cache 폴더에 저장.
        let cacheDir = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first!
        let jsonFilePath = cacheDir.appendingPathComponent(fileName)
        return jsonFilePath
    }
}
